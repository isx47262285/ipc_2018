# /usr/bin/python
#-*- coding: utf-8-*-
#
# exemple-popen.py
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
import sys, argparse 
from subprocess import Popen , PIPE

parser = argparse.ArgumentParser(description=\
        """Exemple popen""")
parser.add_argument("ruta",type=str,\
        help="directori a llistar")
args=parser.parse_args()
# -------------------------------------------------------

command=["ls" , args.ruta]

pipedata = Popen(command, stdout=PIPE)

for line in pipedata.stdout:  # el subprocess lee desde el pipe 
	print line,

exit(0)

