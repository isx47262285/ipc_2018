# /usr/bin/python
#-*- coding: utf-8-*-
# cal-client-one2oned-pissarra.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
import sys,socket, argparse
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description="""CAL server""")
parser.add_argument("server",type=str)
parser.add_argument("-p","--port",type=int, dest="port", default=50001)
args=parser.parse_args()

HOST = args.server
PORT = args.port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
command = ["ps ax"]
pipedata = Popen(command, stdout=PIPE , shell=True)

for line in pipedata.stdout:
	s.send(line)	
s.close()
sys.exit(0)
