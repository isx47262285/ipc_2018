#!/usr/local/bin/python
#-*- coding: utf-8-*-
# descripcion:
#    08-sort-users.py [-s login|gid] file
#			Carregar en una llista a memòria els usuaris provinents d'un fitxer
#			tipus /etc/passwd, usant objectes UnixUser, i llistar-los.
#			Ordenar el llistat (stdout) segons el criteri login o el criteri
#			gid (estable).
# synopsis:
# 		[file]
#################################################################################################################################
###################  DEFINIR ELS ARGUMENTS   ###################################################################################
import argparse
parser = argparse.ArgumentParser(description="exemple de procesar arguments ", prog= "exemple.py", epilog = " thats all folks")
#parser.add_argument("-n", "--numline" , type = int , help = "numline (int)", dest = "MAXLIN" ,metavar = "num_lineas" ,default = 10)
parser.add_argument (type = str , help= "fitxer a ordenar" , metavar = "elfitxer" , dest = "filein")
parser.add_argument ("-s", "--sort" , type = str , help = "option to sort" , metavar= "sort" , default = "login", dest = "sort" , choices=["login","gid"])
args= parser.parse_args()
####################################################################################
# definicio de la clase 
####################################################################################
class UnixUser():
  """Classe UnixUser: prototipus de /etc/passwd
  login:passwd:uid:gid:gecos:home:shell"""
  def __init__(self,userline):
    "Constructor objectes UnixUser"
    userfield=userline.split(":")
    self.login=userfield[0]
    self.passwd=userfield[1]
    self.uid=int(userfield[2])
    self.gid=int(userfield[3])
    self.gecos=userfield[4]
    self.home=userfield[5]
    self.shell=userfield[6]
  def show(self):
	"mostra les dades del usuari "
	print "login: %s, passwd: %s, uid:%d, gid=%d, gecos=%s , home=%s, shell=%s" % (self.login, self.passwd, self.uid, self.gid, self.gecos, self.home, self.shell)
  def __str__(self):
	"printa str user"
	return "%s %s %d %d %s %s %s" % (self.login, self.passwd, self.uid, self.gid, self.gecos, self.home, self.shell)
	  
########################################################################
###  DEFINIR EL CMP DEL SORT #####################
########################################################################
def cmp_login(a,b):
  '''Comparador d'usuaris segons el login'''
  if a.login > b.login:
    return 1
  if a.login < b.login:
    return -1
  return 0

def cmp_gid(a,b):
  '''Comparador d'usuaris segons el gid'''
  if a.gid > b.gid:
    return 1
  if a.gid < b.gid:
    return -1
  if a.login > b.login:
    return 1
  if a.login < b.login:
    return -1
  return 0

###################################################################################################################
#programa que usa un tractament de objectes a un fitxer per parsejar usuaris del sistema 
###################################################################################################################
counter = 0
listuser = []
fitxer = args.filein
ff = open(fitxer , 'r')

for linea in ff:
	user=UnixUser(linea)
	listuser.append(user)
	
ff.close()
# ORDENACION

if args.sort == "login":
	listuser.sort(cmp=cmp_login)
else:
	listuser.sort(cmp=cmp_gid)

for user in listuser:
	print user
	
exit(0)
