# /usr/bin/python
#-*- coding: utf-8-*-
#
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# ------------------------------------
#Atacar la database indicada, llistar cada un dels registres del codi indicat.
#Explicacio dels diàlegs, un read o readlines es queda encallat at infinitum
#quan el subprocess no finalitza. Cal llegir exactament les n linies de resposta
#(dues si ok, una si error), pero no infinites.
#
#Exemple amb wc < file i wc de stdin.
# synopsis:
# 		14-popen-sql-multi.py  -d database   [-c numclie]...
###################################################################################################
import sys, argparse 
from subprocess import Popen , PIPE

parser = argparse.ArgumentParser(description="""Exemple popen""")
parser.add_argument("-d","--database", help='database SQL a consultar',metavar='databaseSQL', default="training", dest="database")
parser.add_argument("-c","--client", help='num_cliente', type=int ,metavar='num_clie', dest= "llista", action="append" , required=True)
args=parser.parse_args()
###################################################################

command = "psql -qtA -F',' -h 172.17.0.2 -U postgres "+ args.database
print command
pipedata = Popen(command, stdout=PIPE ,stdin = PIPE, stderr=PIPE, shell=True)

for num_clie in args.llista:
	
	sentencia = "select * from clientes where num_clie=%d;" % (num_clie)
	print sentencia
	pipedata.stdin.write(sentencia+"\n")
	
	print pipedata.stdout.readline()
	

sys.exit(0)
