# /usr/bin/python
#-*- coding: utf-8-*-
#
# signal.py  segons
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------

import sys, os

print " hola començamente de prg principal"

print "PID pare: ", os.getpid()

pid = os.fork()  # retorna el pid del fill i un cero

if pid != 0:
	#os.wait()  #esta fet perque se esperi a que acaben els seus fills
	print "\t prog pare : " , os.getpid() , pid
	sys.exit(0)
	
print "prog fill : " , os.getpid() , pid
dic = {"nom":"pere", "edad":"15"}

#os.execv("/usr/bin/ls" , ["/usr/bin/ls" ,"-ls" , "/"])
#os.execl("/usr/bin/ls" , "/usr/bin/ls" ,"-la" , "/")
#os.execl("/usr/bin/ls" , "ls" ,"-la" , "/")   # fa lo mateix que el de abans pero ruta relativa
#os.execlp("ls" , "ls" ,"-la" , "/" )  # fa servir el path 
os.execve("/usr/bin/echo" , ["/usr/bin/echo" , "echo $nom"  ] , dic)
print "hasta luego " # no surtira

sys.exit(0)
