#!/usr/local/bin/python
#-*- coding: utf-8-*-
# descripcion:
#
# LListar els grups del sistema ordenats pel criteri de gname, gid o de número d'usuaris.
# Atenció cal gestionar apropiadament la duplicitat dels usuaris en un grup.
#		Requeriment: desar a la llista d'usuaris del grup tots aquells usuaris
#		que hi pertanyin, sense duplicitats, tant com a grup principal com a grup secundari.
# synopsis:
# 		10-count-by-group.py [-s gid | gname | nusers ] -u usuaris -g grups
#################################################################################################################################
###################  DEFINIR ELS ARGUMENTS   ###################################################################################
import sys, argparse
parser = argparse.ArgumentParser(description="exemple de procesar arguments ", prog= "exemple.py", epilog = " thats all folks")
#parser.add_argument("-n", "--numline" , type = int , help = "numline (int)", dest = "MAXLIN" ,metavar = "num_lineas" ,default = 10)
parser.add_argument ("-u" , type = str , help= "fitxer de usuaris " , metavar = "fileusers" , dest = "fileusers")
parser.add_argument ("-g" , type = str , help= "fitxer de groups " , metavar = "filegroup" , dest = "filegroup")
parser.add_argument ("-s", "--sort" , type = str , help = "option to sort" , metavar= "criteri_sort" , dest = "sort" , choices=["gid","gname","nusers"])
args= parser.parse_args()

class UnixUser():
  """Classe UnixUser: prototipus de /etc/passwd
  login:passwd:uid:gid:gecos:home:shell"""
  def __init__(self,userline):
    "Constructor objectes UnixUser"
    userfield=userline.split(":")
    self.login=userfield[0]
    self.passwd=userfield[1]
    self.uid=int(userfield[2])
    self.gid=int(userfield[3])
    self.gname=""
    if self.gid in groupdic:
		self.gname= groupdic[self.gid].gname
    self.gecos=userfield[4]
    self.home=userfield[5]
    self.shell=userfield[6]
  def show(self):
	"mostra les dades del usuari "
	print "login: %s, passwd: %s, uid:%d, gid=%d, gname=%s, gecos=%s , home=%s, shell=%s" % (self.login, self.passwd, self.uid, self.gid ,self.gname, self.gecos, self.home, self.shell)
  def __str__(self):
	"printa str user"
	return "%s %s %d %d %s %s %s %s" % (self.login, self.passwd, self.uid, self.gid, self.gname , self.gecos, self.home, self.shell)

class UnixGroup():
	"""Classe UnixGroup: prototipus de /etc/group gname_passwd:gid:listUsers"""
	def __init__(self,groupline):
	  groupfield=groupline.split(":")
	  self.gname=groupfield[0]
	  self.passwd=groupfield[1]
	  self.gid=int(groupfield[2])
	  self.listuserstr=groupfield[3]
	  self.userlist=[]
	  if self.listuserstr[:-1]:
		  self.userlist=self.listuserstr[:-1].split(",")
	def __str__(self):
	  "printa bunic unixgroup"
	  return "%s %s %d %s" % (self.gname, self.passwd, self.gid, self.userlist)
	  
######################################################################
#################  PROGRAMA ##################
listusers=[]
groupdic={}

ff_filegroup = open( args.filegroup, "r")

for linea in ff_filegroup:
	group = UnixGroup(linea)
	groupdic[group.gid]= group 
	
ff_filegroup.close()
# creamos la lista de usuarios
ff_fileusers = open( args.fileusers, "r")

for linea in ff_fileusers:
	user = UnixUser(linea)
	listusers.append(user)
	if user.gid in groupdic:
		if user.login not in groupdic[user.gid].userlist:
			groupdic[user.gid].userlist.append(user.login)
ff_fileusers.close()

# criterio de ordenacion (index)
index =[]

if args.sort== "gid":
	index = groupdic.keys()
	
elif args.sort == "gname":
	index = [ (groupdic[k].gname, k) for k in groupdic ]
	
elif args.sort == "nusers":
	index = [ (len(groupdic[k].userlist),k) for k in groupdic ]
	
index.sort()

if args.sort == "gid":
	for k in index :
		print groupdic[k]
		
else:
	for _ignore,k in index:
		print groupdic[k]
'''		
else:
	for t in index:
		print groupdic[t[1]]
'''	
