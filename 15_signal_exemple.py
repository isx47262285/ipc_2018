# /usr/bin/python
#-*- coding: utf-8-*-
#
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# ------------------------------------
#Exemple amb wc < file i wc de stdin.
# synopsis:
# 		15_signal_exemple.py
###################################################################################################
import sys, os,signal
def myhandler(signum,frame):
	print "signal handler" , signum
	print "hasta luego lucas"
	sys.exit(1)

def mydeath(signum,frame):
	print "signal handler" , signum
	print "que te mueras tu "
	
	
signal.signal(signal.SIGALRM,myhandler)
signal.signal(signal.SIGUSR2,myhandler)
signal.signal(signal.SIGUSR1,mydeath)
signal.signal(signal.SIGTERM,signal.SIG_IGN)
signal.signal(signal.SIGINT,signal.SIG_IGN)
signal.alarm(180)

print os.getpid()
while True:
		# fem un open que es queda encallat
		pass

signal.alarm(60)

sys.exit(0)
