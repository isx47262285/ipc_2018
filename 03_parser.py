#!/usr/local/bin/python
#-*- coding: utf-8-*-
# descripcion:
#       programas de repas de python 
# synopsis:
# 		01-head[file]
#################################################################################################################################
###################  DEFINIR ELS ARGUMENTS   ###################################################################################
import argparse

parser = argparse.ArgumentParser(description="exemple de procesar arguments ", prog= "exemple.py", epilog = " thats all folks")

parser.add_argument("-n", "--numline" , type = int , help = "numline (int)", dest = "MAXLIN" ,metavar = "num_lineas" ,default = 10)

parser.add_argument("-f", "--fit" , type = str , help = "fitxer a procesar" , metavar= "elfitxer" , default = "/dev/stdin", dest = "filein"  )


args= parser.parse_args()

print parser

print args

###################################################################################################################

counter = 0

ff = open(args.filein , 'r')

for linea in ff:
	counter += 1
	print linea
	if counter == args.MAXLIN : break 
ff.close()


exit(0)
