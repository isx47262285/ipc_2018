# /usr/bin/python
#-*- coding: utf-8-*-
#
# exemple-echoClient.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
import sys,socket
HOST = ''
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

s.bind((HOST,PORT))
s.listen(1) # el progra es quedara clavat
conn , addr  = s.accept()   # conexio: adreça
print " conected by  , ",  addr
while True:
	data = conn.recv(1024)
	if not data: break 
	conn.send(data)
conn.close()

sys.exit(0)

