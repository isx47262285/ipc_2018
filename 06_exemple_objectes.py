# /usr/bin/python
#-*- coding: utf-8-*-
#
# Exemple de creació de classes i objectes
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
class UnixUser():
  """Classe UnixUser: prototipus de /etc/passwd
  login:passwd:uid:gid:gecos:home:shell"""
  def __init__(self,l,i,g):
    "Constructor objectes UnixUser"
    self.login=l
    self.uid=i
    self.gid=g
  def show(self):
	"mostra les dades del usuari "
	print "login: %s, uid:%d, gid=%d" % (self.login, self.uid, self.gid)
  def sumaun(self):
    "funcio tonta que suma un al uid"
    self.uid+=1
  def __str__(self):
	"printa str user"
	return "%s %d %d" % (self.login, self.uid, self.gid)
	  
user1.show()
user1.sumaun()
user1.show()
user1
l=[13,"juny",user1,user2]
l[2].show()

