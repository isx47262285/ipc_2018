# /usr/bin/python
#-*- coding: utf-8-*-
#
# exemple-echoClient.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
#  22.daytime_server.py client
# -------------------------------------
import sys,socket, os , signal, argparse
from subprocess import Popen, PIPE
# definir args
parser = argparse.ArgumentParser(description="Gestionar l'alarma")
parser.add_argument("-y","--year", type=int, dest = "year", help="year to found" , default = "2019")
parser.add_argument("-p","--port", type=int, default = "50001")
args=parser.parse_args()
########################################################################################
import sys,socket
HOST = ''
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
# ojo podria haber un bucle while, no la fem servir perque reb una sola emisio
data = s.recv(1024)

s.close()

#print "recived by " , repr(data)
print "recived by " , str(data)
sys.exit(0)
