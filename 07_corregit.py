#!/usr/local/bin/python
#-*- coding: utf-8-*-
# descripcion:
#     07 iteracio de tractamen de objectes
# synopsis:
# 		[file]
#################################################################################################################################
###################  DEFINIR ELS ARGUMENTS   ###################################################################################
import argparse
parser = argparse.ArgumentParser(description="exemple de procesar arguments ", prog= "exemple.py", epilog = " thats all folks")
#parser.add_argument("-n", "--numline" , type = int , help = "numline (int)", dest = "MAXLIN" ,metavar = "num_lineas" ,default = 10)
parser.add_argument("-f", "--fit" , type = str , help = "fitxer a procesar" , metavar= "elfitxer" , default = "/dev/stdin", dest = "filein"  )
args= parser.parse_args()
# definicio de la clase 
class UnixUser():
  """Classe UnixUser: prototipus de /etc/passwd
  login:passwd:uid:gid:gecos:home:shell"""
  def __init__(self,userline):
    "Constructor objectes UnixUser"
    userfield=userline.split(":")
    self.login=userfield[0]
    self.passwd=userfield[1]
    self.uid=int(userfield[2])
    self.gid=int(userfield[3])
    self.gecos=userfield[4]
    self.home=userfield[5]
    self.shell=userfield[6]
  def show(self):
	"mostra les dades del usuari "
	print "login: %s, passwd: %s, uid:%d, gid=%d, gecos=%s , home=%s, shell=%s" % (self.login, self.passwd, self.uid, self.gid, self.gecos, self.home, self.shell)
  def sumaun(self):
    "funcio tonta que suma un al uid"
    self.uid+=1
  def __str__(self):
	"printa str user"
	return "%s %s %d %d %s %s %s" % (self.login, self.passwd, self.uid, self.gid, self.gecos, self.home, self.shell)
	  

###################################################################################################################
#programa que usa un tractament de objectes a un fitxer per parsejar usuaris del sistema 
###################################################################################################################
counter = 0
listuser = []
ff = open(args.filein , 'r')

for linea in ff:
	user=UnixUser(linea)
	listuser.append(user)	
ff.close()

print "----------------------------------------------------"
print "lista de users"
print "----------------------------------------------------"
print listuser
print 
# printariem un a un los usuaris ja en la llista
print "-----------------------------------------------------"
print "iteracio de users" 
print "-------------------------------------------------------"
print 
for user in listuser:
	print user
	
exit(0)
