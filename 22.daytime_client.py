# /usr/bin/python
#-*- coding: utf-8-*-
#
# exemple-echoClient.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
#  22.daytime_server.py client
# -------------------------------------
import sys,socket
HOST = ''
PORT = 13
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))

# ojo podria haber un bucle while, no la fem servir perque reb una sola emisio

data = s.recv(1024)

s.close()

print "recived by " , repr(data)

sys.exit(0)
