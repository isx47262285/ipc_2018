# /usr/bin/python
#-*- coding: utf-8-*-
#
# signal.py  segons
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# prog exec  que fa un proces fill que es mort 
# y el process fill crida un proces fork 
# -------------------------------------

import sys, os , signal

print " hola començamente de prg principal"

print "PID pare: ", os.getpid()

pid = os.fork()  # retorna el pid del fill i un cero

if pid != 0:
	#os.wait()  #esta fet perque se esperi a que acaben els seus fills
	print "\t prog pare : " , os.getpid() , pid
	sys.exit(0)
	
print "prog fill : " , os.getpid() , pid

os.execv("/usr/bin/python" , ["/usr/bin/python" ,"16_signal_profe.py" , "180"])

print "bye"
sys.exit(0)
