# /usr/bin/python
#-*- coding: utf-8-*-
#
# 26.telnet_server.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
import sys,socket,argparse, signal,os,datetime
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description=\
        """CAL server""",\
        epilog="thats all folks")
parser.add_argument("-d","--debug",type=int,\
        help="mostrar la conexio",dest="debug",\
        metavar="debug",default=0)
parser.add_argument("-p","--puerto",type=int,\
        help="numero de puerto", metavar="puerto",\
        default=50005,dest="puerto")
args=parser.parse_args()
#-------------------------------------------------------------------
pid=os.fork()
if pid!=0:
	print "Engegat el server:",pid
	sys.exit(0)
	
llistaPeers=[]
HOST = ''
PORT = args.puerto

#generem socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#per reusar la coneccio i utilitzar el mateix port
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
#En qui port i adreça estarà lligat
s.bind((HOST,PORT))

#escoltem
s.listen(1)
conn,addr = s.accept()

while True:
	#conneccio i adreça de cada client
	print "conected" , addr 
	while True:
		data = conn.recv(1024)
		pipedata = Popen(data, stdout = PIPE, stdin = PIPE,  shell = True)
		
		for line in pipedata.stdin:
			conn.send(line)
			conn.send("yata")
	conn.close()
sys.exit(0)
