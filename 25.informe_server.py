# /usr/bin/python
#-*- coding: utf-8-*-
#
# exemple-echoClient.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
#  22.daytime_server.py client
# -------------------------------------
import sys,socket,os,signal,argparse
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description="""CAL server""")
parser.add_argument("-a","--any",type=int, default=2019)
parser.add_argument("-p","--port",type=int, default=50001)
args=parser.parse_args()
llistaPeers=[]
HOST = ''
PORT = args.port
ANY = args.any
########################################################################################
pid=os.fork()
if pid !=0:
  print "Engegat el server CAL:", pid
  sys.exit(0)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)
while True:
  conn, addr = s.accept()
  print "Connected by", addr
  data = conn.recv(1024)
  for line in data:
    print line
  conn.close()
