#!/usr/local/bin/python
#-*- coding: utf-8-*-
# nombre: roberto altamirano martinez
# isx47262285
# descripcion:
#		
#			LListar els grups del sistema ordenats pel criteri de gname, gid o de
#			número d'usuaris. Atenció cal gestionar apropiadament la duplicitat dels usuaris en un grup.
#		Requeriment: desar a la llista d'usuaris del grup tots aquells usuari que hi pertanyin, sense duplicitats, tant com a grup principal com a
#						grup secundari.
#    
# synopsis:
# 		10-count-by-group.py [-s gid | gname | nusers ] -u usuaris -g grups
#################################################################################################################################
###################  DEFINIR ELS ARGUMENTS   ###################################################################################
import argparse
parser = argparse.ArgumentParser(description="exemple de procesar arguments ", prog= "exemple.py", epilog = " thats all folks")
#parser.add_argument("-n", "--numline" , type = int , help = "numline (int)", dest = "MAXLIN" ,metavar = "num_lineas" ,default = 10)
parser.add_argument ("-u" , "--fileuser" ,type = str , help= "fitxer de usuaris" , metavar = "usuaris" , dest = "usuaris")
parser.add_argument ("-s", "--sort" , type = str , help = "option to sort" , metavar= "criterio sort" , dest = "sort" , choices=["login","gname","nusers"])
args= parser.parse_args()


#######################DEFINIR LOS CONSTRUCTORES ########################################
class UnixUser():
  """Classe UnixUser: prototipus de /etc/passwd
  login:passwd:uid:gid:gecos:home:shell"""
  def __init__(self,userLine):
    "Constructor objectes UnixUser"
    userField=userLine.split(":")
    self.login=userField[0]
    self.passwd=userField[1]
    self.uid=int(userField[2])
    self.gid=int(userField[3])
    self.gname=""
    if self.gid in groupDict:
      self.gname=groupDict[self.gid].gname
    self.gecos=userField[4]
    self.home=userField[5]
    self.shell=userField[6]
  def show(self):
    "Mostra les dades de l'usuari"
    print "login: %s, uid:%d, gid=%d" % \
           (self.login, self.passwd, self.uid, self.gid, self.gname,\
            self.gecos, self.home, self.shell)
            
	def __str__(self):
    "functió to_string d'un objcete UnixUser"
    return "%s %d %d %s" % (self.login, self.uid, self.gid, self.gname)

class UnixGroup():
  """Classe UnixGroup: prototipus de /etc/group
  gname_passwd:gid:listUsers"""
  def __init__(self,groupLine):
    "Constructor objectes UnixGroup"
    groupField = groupLine.split(":")
    self.gname = groupField[0]
    self.passwd = groupField[1]
    self.gid = int(groupField[2])
    self.userListStr = groupField[3]
    self.userList=[]
    if self.userListStr[:-1]:
      self.userList = self.userListStr[:-1].split(",")
  def __str__(self):
    "functió to_string d'un objecte UnixGroup"
    return "%s %d %s" % (self.gname, int(self.gid), self.userList)
    
    
    
################### PROGRAMA ##########################################

groupFile=open(args.groupFile,"r")
for line in groupFile:
  group=UnixGroup(line)
  groupDict[group.gid]=group
groupFile.close()
userFile=open(args.userFile,"r")
userList=[]
for line in userFile:
  user=UnixUser(line)
  userList.append(user)
userFile.close()
if args.criteria=="login":
  userList.sort(cmp_login)   # CAMBIAR POR UNA TUPLA 
elif args.criteria=="gid":
  userList.sort(cmp_gid)
else:
  userList.sort(cmp_gname)
for user in userList:
 print user
exit(0)
