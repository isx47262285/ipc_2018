# /usr/bin/python
#-*- coding: utf-8-*-
#
# exemple-echoClient.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# 22.daytime_server.py
# -------------------------------------
import sys,socket, os , signal, argparse
from subprocess import Popen, PIPE
# definir args
parser = argparse.ArgumentParser(description="Gestionar l'alarma")
parser.add_argument("-y","--year", type=int, dest = "year", help="year to found")
#parser.add_argument("-p","--port", type=int, dest = "port", help="port  to connect")
args=parser.parse_args()

llistapeers=[]

def mysigusr1(signum,frame):
  print "hola radiola:", signum
  print len(llistapeers)
  sys.exit()
def mysigusr2(signum,frame):
  print "hasta luego primo:", signum
  print len(llistapeers)
  sys.exit(0)

def mysigterm(signum,frame):
  print "terminator:", signum
  print len(llistapeers)
  sys.exit(0)

# programa
signal.signal(signal.SIGUSR1,mysigusr1)
signal.signal(signal.SIGUSR2,mysigusr2)
signal.signal(signal.SIGTERM,mysigterm)
print "SERVIDOR PARE ENGEGAT"

pid = os.fork()  # retorna el pid del fill i un cero

if pid != 0:
	#os.wait()  #esta fet perque se esperi a que acaben els seus fills
	print "\t ENGEGAT EL SERVER CAL  : " , pid
	sys.exit(0)

HOST = ''
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1) # el progra es quedara clavat
while True:
	conn,addr  = s.accept()   # conexio: adreça
	print "conected by: " , addr 
	llistapeers.append(addr)
	# un read provinent de un pipe amb la informacio del client
	command = ["cal %d" % (args.year)]
	pipedata = Popen(command, stdout=PIPE )
	for line in pipedata.stdout:
		conn.send(line)
		
	conn.close()
	
