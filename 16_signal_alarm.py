# /usr/bin/python
#-*- coding: utf-8-*-
#
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# ------------------------------------
#Exemple amb wc < file i wc de stdin.
# synopsis:
# 	16_signal_exemple.py
# https://gitlab.com/edtasixm06/ipc-2018-19/blob/master/16-signal.py
###################################################################################################

import sys, os,signal, argparse


parser = argparse.ArgumentParser(description="""augment y decrement""")
parser.add_argument(help='segons a augmentar o decrementar',metavar='segons' , type=int , dest="alarm")
args=parser.parse_args()
# global variables 
augments = 0
decrements = 0
actual =0

def myhandler(signum,frame):
	global augments, decrements
	print "signal handler" , signum
	print "hasta luego lucas"
	print augments , decrements
	sys.exit(1)

def augment(signum,frame):
	print "signal augement" , signum
	print "augment 60s"
	#actual = args.alarm + 60
	actual = signal.alarm(0)
	signal.alarm(actual + 60)
	augments += 1
	

def decrement(signum,frame):
	print "signal decrement" , signum
	print "decrement 60s"
	#actual = args.alarm - 60
	actual =signal.alarm(0)
	if actual -60<0:
		
	decrement +=1

def reinicio(signum,frame):
	print "reinicio"
	actual = args.alarm
	signal.alarm(actual)


def printar(signum,frame):
	print "alarma: " , actual
	print "ups: " , augments , "downs: " , decrements 

signal.signal(signal.SIGUSR1,augment)
signal.signal(signal.SIGUSR2,decrement)
signal.signal(signal.HUB,reinicio)
signal.signal(signal.TERM,printar)
signal.signal(signal.SIGALRM,myhandler)


print os.getpid()
while True:
		# fem un open que es queda encallat
		pass

sys.exit(0)
