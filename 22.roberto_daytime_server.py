# /usr/bin/python
#-*- coding: utf-8-*-
#
# exemple-echoClient.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# 22.daytime_server.py
# -------------------------------------
import sys,socket
from subprocess import Popen, PIPE

HOST = ''
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1) # el progra es quedara clavat
conn,addr  = s.accept()   # conexio: adreça
print "conected by: " , addr 

# un read provinent de un pipe amb la informacio del client
command = ["date"]
pipedata = Popen(command, stdout=PIPE )

for line in pipedata.stdout:
	conn.send(line)	
conn.close()







