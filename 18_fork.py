# /usr/bin/python
#-*- coding: utf-8-*-
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
#	18-fork-signal.py
#	Usant el programa d'exemple fork fer que el procés fill (un while infinit) es
#	governi amb senyals. Amb siguser1 mostra "hola radiola" i amb sigusr2 mostra
#		"adeu andreu" i finalitza.
#######################################################################################
import sys, os, signal
# funcio signal

def mysigusr1(signum,frame):
  print "hola radiola:", signum

def mysigusr2(signum,frame):
  print "hasta luego primo:", signum
  sys.exit(0)

# programa

signal.signal(signal.SIGUSR1,mysigusr1)
signal.signal(signal.SIGUSR2,mysigusr2)

print " hola començamente de prg principal"

print "PID pare: ", os.getpid()

pid = os.fork()  # retorna el pid del fill i un cero

if pid != 0:
	#os.wait()  #esta fet perque se esperi a que acaben els seus fills
	print "\t prog pare : " , os.getpid() , pid
	print "hasta luego lucas"
	sys.exit(0)

print "soc el fill : ", os.getpid() , pid 


while True:
	pass
